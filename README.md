# UFRB Eventos (ufrbeventos)

Aplicativo de gerenciamento de Eventos da Universidade Federal do Recôncavo da Bahia

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).


### Foram adicionados na criação os seguintes pacotes do Vue.js
```
Vuex
Axios
```
